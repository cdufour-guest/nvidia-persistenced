nvidia-persistenced (390.25-1) unstable; urgency=medium

  * New upstream release.
  * Add debian/upstream/metadata.
  * Fix new Lintian issues.
  * Switch Vcs-* URLs to salsa.debian.org.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 06 Mar 2018 04:13:59 +0100

nvidia-persistenced (384.111-1~deb9u1) stretch; urgency=medium

  * Rebuild for stretch.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 26 Feb 2018 00:36:01 +0100

nvidia-persistenced (384.111-1) unstable; urgency=medium

  * New upstream release.
  * B-D: dpkg-dev (>= 1.18.8) for SOURCE_DATE_EPOCH in pkg-info.mk.
  * Bump Standards-Version to 4.1.3. No changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 13 Jan 2018 18:40:41 +0100

nvidia-persistenced (384.98-1) unstable; urgency=medium

  * New upstream release.
  * Use https:// URL in the watch file.
  * Bump Standards-Version to 4.1.1. No changes needed.
  * Set Rules-Requires-Root: no.
  * Use dpkg makefile snippets instead of manual changelog parsing.
  * Use a fixed OUTPUTDIR for improved reproducibility.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 17 Nov 2017 02:50:48 +0100

nvidia-persistenced (375.26-2) unstable; urgency=medium

  * Add Depends: libnvidia-cfg1 | libnvidia-cfg1-any, thanks to Gero Müller.
  * Do not build for ppc64el which does not have a curent driver packaged.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 18 Jan 2017 16:16:20 +0100

nvidia-persistenced (375.26-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper compat level 10.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 13 Jan 2017 03:21:35 +0100

nvidia-persistenced (367.57-1) unstable; urgency=medium

  * New upstream release.
    - Fixed a bug in nvidia-persistenced that caused it to incorrectly delete
      the PID file if a second instance of the daemon is started.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 15 Oct 2016 16:28:30 +0200

nvidia-persistenced (367.44-1) unstable; urgency=medium

  * New upstream release.
    - Changed the behavior of nvidia-persistenced to enable persistence mode
      by default for all GPUs when the daemon is started.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 08 Oct 2016 15:10:39 +0200

nvidia-persistenced (367.18-1) unstable; urgency=medium

  * New upstream release.
    * Allows overriding the stripping options.
  * Enable more hardening.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 30 May 2016 09:53:44 +0200

nvidia-persistenced (364.15-2) unstable; urgency=medium

  * Multi-Arch: foreign, not allowed.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 19 Apr 2016 23:45:13 +0200

nvidia-persistenced (364.15-1) unstable; urgency=medium

  * New upstream release.
  * Mark as Multi-Arch: allowed.
  * Simplify rules and clean up properly.
  * Fix upstream build system to allow creation of unstripped binaries.
  * Bump Standards-Version to 3.9.8. No changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 19 Apr 2016 23:24:39 +0200

nvidia-persistenced (361.28-1) unstable; urgency=medium

  * New upstream release.
  * Build for ppc64el.
  * Update Vcs-Git URL.
  * Bump Standards-Version to 3.9.7. No changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 17 Feb 2016 01:15:17 +0100

nvidia-persistenced (358.09-1) unstable; urgency=medium

  * New upstream release.
  * Fix Vcs-Browser URL.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 08 Nov 2015 02:22:34 +0100

nvidia-persistenced (352.21-1) unstable; urgency=medium

  * New upstream release.
  * Update d/copyright: Upstream source is consistently licensed under Expat
    since 349.12, previously common-utils/* was under GPL-2.
  * d/rules: Make the build reproducible by using deterministic timestamp,
    user and hostname strings to be included in the binary. Based on patch for
    nvidia-settings by Jérémy Bobbio.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 21 Jun 2015 17:28:54 +0200

nvidia-persistenced (343.13-1) unstable; urgency=low

  * Initial release.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 29 Oct 2014 12:54:27 +0100
